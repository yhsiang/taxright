# Taxright Poll Robot

人權大調查

# Dependencies

it based on nodejs 

* querystring
* csv

# Dev Denpendencies

* LiveScript
* browserify

# Usage

## 如果你擁有unix-like 的環境

1. 安裝nodejs
2. npm install
3. 執行以下動作
```
$ ./run.sh
```

## 如果你不知道上面在說什麼

1. 安裝chrome
2. 打開chrome並連到 [人權調查網頁](http://www.tpccool.url.tw/survey.php)
3. 開啓javascript控制台
	* windows 請使用 ctrl+shift+j 
	* mac 請使用 alt+command+j
	* 或者在工具裡面開啟
4. 將[bundle.js](https://bitbucket.org/yhsiang/taxright/raw/8961915d5a466b238d6100babbf56db53c67df2f/bundle.js)的內容複製貼到控制台裡面按enter
5. 請等候10秒
6. 看到許多"謝謝您"的msg代表成功, 每10秒會刷新一次
7. 請不要關閉這個分頁或瀏覽器, 重新開啟後請再一次執行2-6的動作