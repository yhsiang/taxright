http = require \http
querystring = require \querystring
fs = require \fs
csv = require \csv

do-survey = (post-data, cb)->
	user-agent =
		* 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36'
		* 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36'

	random = Math.floor Math.random! * (user-agent.length + 1)

	option = do
		host: \www.tpccool.url.tw
		path: \/surveyDo.php
		method: \POST
		headers: do
			'User-Agent': user-agent[random]
			'Content-Type': 'application/x-www-form-urlencoded'
			'Content-Length': post-data.length

	req = http.request option, (res) ->
		res.setEncoding \utf8
		res.on \data, ->
			if it.match /謝謝您/
				cb \success
			else
				cb \failed

	req.write post-data
	req.end!

csv!from.path __dirname + '/data.csv', do
	delimiter: ','
	escape: '"'
	columns: true
.on \record, (row, index)->
	post-data = querystring.stringify row
	do-survey post-data, ->
		if it is \success
			console.log '第 '+ index + ' 筆完成'
		else
			console.log '第 '+ index + ' 筆失敗'

